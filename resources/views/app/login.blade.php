@extends('app.layouts.master')


@section('content')
<h1>Login</h1>

{!! Form::open() !!}
	
	<label>Username</label><br />
	{!! Form::text('username', null) !!}
	<br /><br />

	<label>Password</label><br />
	{!! Form::password('password')!!}
	<br /><br />
	
	{!! Form::submit('Login')!!}
	
{!! Form::close() !!}
@stop