@extends('app.layouts.master')


@section('content')
<h1>New Photo</h1>

{!! Form::open(['files' => 'true']) !!}
	
	<label>Image</label><br />
	{!! Form::file('image') !!}
	<br /><br />

	<label>Description</label><br />
	{!! Form::textarea('description', null) !!}
	<br /><br />

	{!! Form::submit('Create Photo')!!}
{!! Form::close() !!}
@stop