@extends('app.layouts.master')

@section('content')

<h1>Photo App</h1>

@if($photos)
	@foreach($photos as $photo)
		<a href="{{ route('view_photo',$photo->id) }}"><img src="{{ asset($photo->image) }}" class="img-responsive"><br /></a>
		<article>{{ $photo->description }}</article>
		<hr />
	@endforeach
@endif

@if(auth()->user())
	<a href="{{ route('logout') }}">Logout</a>
@else
<a href="{{ route('login') }}">Login</a> | <a href="{{ route('register') }}">Register</a>
@endif
@stop