@extends('app.layouts.master')


@section('content')
<h1>Registration</h1>
@if(session()->has('success'))
	{{ session()->get('success') }}
@endif
{!! Form::open() !!}

Username: {!! Form::text('username', null) !!} <br>
Password: {!! Form::password('password') !!} <br>

{!! Form::submit('register') !!}

{!! Form::close() !!}

@stop