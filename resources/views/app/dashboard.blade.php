@extends('app.layouts.master')


@section('content')
<h1>Welcome {{ ucfirst(auth()->user()->username) }} | <a href="{{ route('logout') }}">Logout</a></h1>

<hr />
<a href="{{ route('newPhoto') }}">+ New Photo</a>
@stop