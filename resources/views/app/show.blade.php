@extends('app.layouts.master')


@section('content')
<img src="{{ asset($photo->image) }}" class="img-responsive">
<br>

<h3>Comments</h3>
@foreach($comments as $comment)
	<p><b>{{ $comment->hasUser($comment->user_id)->username}}</b> :{{ $comment->comment }}</p>
@endforeach
<hr />
@if(auth()->user())
	{!! Form::open(['url' => route('comment')]) !!}
	{!! Form::hidden('photo_id', $photo->id)!!}
	{!! Form::textarea('comment') !!}
	<br><br>
	{!! Form::submit('post') !!} 
	<br><br>
	{!! Form::close() !!}
@else
	<a href="{{ route('homepage')}}">Homepage</a>
@endif
@stop