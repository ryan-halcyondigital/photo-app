<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Comment extends Model
{

	protected $fillable = ['photo_id', 'user_id', 'comment'];
	
	public function hasUser($id)
	{
		return User::find($id);
	}	
}
