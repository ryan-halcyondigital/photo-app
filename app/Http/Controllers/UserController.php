<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    function __construct(User $user)
    {
        $this->user = $user;
    }

    public function login()
    {
    	return view('app.login');
    
    }

    public function attemptLogin(Request $request)
    {	
    	$userCredentials = ['username' => $request->username, 'password' => $request->password];
    	if (auth()->attempt($userCredentials)) {
    		return redirect()->route('dashboard');
    	}
    	else{
    		session()->flash('failed', "Username/Password did not match.");
    		return redirect()->back();
    	}
    }

    public function registration()
    {
        return view('app.registration');
    }

    public function attemptRegister(Request $request)
    {
        $registration = ['username' => $request->username, 'password'=>bcrypt($request->password)];
        $register = $this->user->create($registration);
        session()->flash('success', "Registration Successful.");
        return redirect()->back();
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('homepage');
    }

    public function dashboard()
    {
    	return view('app.dashboard');
    }
}
