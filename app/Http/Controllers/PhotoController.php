<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Photo;
use App\Comment;

class PhotoController extends Controller
{
	function __construct(Photo $photo, Comment $comment)
	{
			$this->photo = $photo;
			$this->comment = $comment;
	}

	public function index()
	{
		$photos = $this->photo->all();
		return view('app.index', compact('photos'));
	}

	public function newPhoto()
	{
		return view('app.new');
	}

	public function createPhoto(Request $request)
	{	
		$imageName = $request->file('image')->getClientOriginalName();
		$newPhoto = ['image' => 'uploads/'.$imageName, 'description' => $request->description];
		$photo = $this->photo->create($newPhoto);
		if ($photo) {
			$moveFile = $request->file('image')->move(public_path().'/uploads/', $imageName);
			return redirect()->route('dashboard');
		}
	}

	public function viewPhoto($id)
	{
		$photo = $this->photo->find($id);
		$comments = $photo->comments;
		return view('app.show',compact('photo', 'comments'));
	}

	public function comment(Request $request)
	{
		$newComment = ['photo_id' => $request->photo_id, 'user_id' => auth()->user()->id, 'comment' => $request->comment];
		$comment = $this->comment->create($newComment);
		return redirect()->route('view_photo', $request->photo_id);
	}
		
}
