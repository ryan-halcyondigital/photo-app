<?php

Route::get('/', ['as' => 'homepage', 'uses' => 'PhotoController@index']);
Route::get('login', ['as' => 'login', 'uses' => 'UserController@login']);
Route::post('login', ['as' => 'attemptLogin', 'uses' => 'UserController@attemptLogin']);
Route::get('register', ['as' => 'register', 'uses' => 'UserController@registration']);
Route::post('register', ['as' => 'attemptRegister', 'uses' => 'UserController@attemptRegister']);
Route::get('logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
Route::get('photo/{id}/view', ['as' => 'view_photo', 'uses' => 'PhotoController@viewPhoto']);

Route::post('photo/comment', ['as' => 'comment', 'uses' => 'PhotoController@comment']);

Route::group(['middleware' => 'auth'], function(){

	Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'UserController@dashboard']);

	Route::get('new/photo', ['as' => 'newPhoto', 'uses' => 'PhotoController@newPhoto']);
	Route::post('new/photo', ['as' => 'createPhoto', 'uses' => 'PhotoController@createPhoto']);
});